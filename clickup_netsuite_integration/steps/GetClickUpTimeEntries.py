from base_steps_syncpack.steps.QueryREST import QueryREST
from ipaascommon import ipaas_exceptions
from ipaascore.BaseStep import BaseStep
from ipaascore import parameter_types
from datetime import datetime, timedelta
from ipaascommon.report_manager import StandardReport
import requests


class GetClickUpTimeEntries(QueryREST):

    def __init__(self):
        self.friendly_name = "Get ClickUp Time Entries"
        self.description = "Get ClickUp Time Entries"
        self.version = "1.0.0"
        self.new_step_parameter(
            name="clickup_api_url_base",
            description="Clickup hostname",
            required=True,
            default_value="https://api.clickup.com",
            param_type=parameter_types.StringParameterShort(),
        )
        self.new_step_parameter(
            name="clickup_team_id",
            description="Clickup team ID",
            required=True,
            default_value="3731021",
            param_type=parameter_types.StringParameterShort(),
        )
        self.new_step_parameter(
            name="clickup_api_token",
            description="ClickUp API token",
            required=True,
            default_value="ams-awesome-authentication-token",
            param_type=parameter_types.StringParameterShort(),
        )
        self.new_step_parameter(
            name="week_of",
            description="Start of the week of Epoch in Milliseconds",
            required=True,
            default_value="",
            param_type=parameter_types.StringParameterShort(),
        )
        self.session = requests.Session()

    def get_folder_customer_uid(self, base_url, task_id, headers):
        task_url = "{}/api/v2/task/{}/?".format(base_url, task_id)
        task_response = self.make_rest_call(getattr(self.session, 'get'), task_url, headers, {}, expected_http_response=200)
        task_result = task_response.json()
        if task_result:
            list_name = task_result["list"]["name"]
            folder_name = task_result["folder"]["name"]
            # folder_customer_uid = "{} - {}".format(folder_name, list_name)
            folder_customer_uid = list_name
        return folder_customer_uid

    def execute(self):
        clickup_api_url_base = self.get_parameter("clickup_api_url_base")
        clickup_team_id = self.get_parameter("clickup_team_id")
        clickup_api_token = self.get_parameter("clickup_api_token")
        week_of = self.get_parameter("week_of")
        self.logger.debug("clickup_api_url_base: {}".format(clickup_api_url_base))
        self.logger.debug("clickup_team_id: {}".format(clickup_team_id))
        self.logger.debug("clickup_api_token: {}".format(clickup_api_token))
        self.logger.debug("week_of: {}".format(week_of))

        dt = datetime.strptime(week_of,'%m/%d/%Y')
        week_start = dt - timedelta(days = (dt.weekday() + 1) % 7)
        week_end = week_start + timedelta(days=6)
        week_start_millisec = int(week_start.timestamp() * 1000)
        week_end_millisec = int(week_end.timestamp() * 1000)

        url = "{}/api/v2/team/{}/time_entries?start_date={}&end_date={}".format(clickup_api_url_base, clickup_team_id, week_start_millisec, week_end_millisec)
        self.logger.debug("url: {}".format(url))

        headers = {"content-type": "application/json","Authorization": clickup_api_token}

        response = self.make_rest_call(getattr(self.session, 'get'), url, headers, {}, expected_http_response=200)
        result = response.json()

        customer_info = dict()
        task_customer_mapping = dict()
        if result:
            for time_entry in result.get("data",[]):
                task = time_entry["task"]
                task_id = task["id"]
                task_name = task["name"]
                description = time_entry["description"]
                notes = description if description else task_name
                start = int(time_entry["start"])
                start_dt = datetime.fromtimestamp(start/1000.0)
                start_date = start_dt.strftime("%m/%d")
                duration = int(time_entry["duration"])
                duration_min_round = round(duration/1000/60)

                if task_id not in task_customer_mapping:
                    folder_customer_uid = self.get_folder_customer_uid(clickup_api_url_base, task_id, headers)
                    task_customer_mapping[task_id] = folder_customer_uid
                else:
                    folder_customer_uid = task_customer_mapping[task_id]

                if folder_customer_uid not in customer_info:
                    customer_info[folder_customer_uid] = dict()
                if start_date not in customer_info[folder_customer_uid]:
                    customer_info[folder_customer_uid][start_date] = dict()
                if "duration" not in customer_info[folder_customer_uid][start_date]:
                    customer_info[folder_customer_uid][start_date]["duration"] = 0
                if "notes" not in customer_info[folder_customer_uid][start_date]:
                    customer_info[folder_customer_uid][start_date]["notes"] = list()
                customer_info[folder_customer_uid][start_date]["duration"] += duration_min_round
                if notes not in customer_info[folder_customer_uid][start_date]["notes"]:
                    customer_info[folder_customer_uid][start_date]["notes"].append(notes)

        
        self.logger.debug("customer_info: {}".format(customer_info))

        self.logger.debug("Create Report")
        day_of_week = list()
        section1 = StandardReport("REPORT_" + self.application_id, self.application_name, "Click Up Timesheet Summary",True)
        section1.add_column(' Customer')
        for i in range(7):
            date_dt = week_start + timedelta(days=i)
            date = date_dt.strftime("%m/%d")
            day_of_week.append(date)
            section1.add_column(date)
        section1.add_column('Total')

        total_min = 0
        for customer, customer_date_info in customer_info.items():
            section1.add_row(' Customer', customer)
            customer_total_min = 0
            for day in day_of_week:
                if day in customer_date_info:
                    duration_min = customer_date_info[day]["duration"]
                    customer_total_min += duration_min
                    duration_hr = '{:02d}:{:02d}'.format(*divmod(duration_min, 60))
                    section1.add_row(day, duration_hr)
                else:
                    section1.add_row(day, "--")
            customer_total_hr = '{:02d}:{:02d}'.format(*divmod(customer_total_min, 60))
            section1.add_row('Total', customer_total_hr)
            total_min += customer_total_min

            section1.add_row(' Customer', customer + ' - Notes')
            for day in day_of_week:
                if day in customer_date_info:
                    notes = "; ".join(customer_date_info[day]["notes"])
                    section1.add_row(day, notes)
                else:
                    section1.add_row(day, "--")
            section1.add_row('Total', "--")

        total_hr = '{:02d}:{:02d}'.format(*divmod(total_min, 60))
        section1.add_row('Total', total_hr)

        self.save_data_for_next_step(customer_info)
